<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<html>
<head title="Список пользователей">
    <script>
        function showUpdateForm(formId) {
            document.getElementById("updateForm" + formId).hidden = false;
            document.getElementById("cancelButton" + formId).hidden = false;
            document.getElementById("deleteForm" + formId).hidden = false;
            document.getElementById("updateButton" + formId).hidden = true;
        }

        function hideUpdateForm(formId) {
            document.getElementById("updateForm" + formId).hidden = true;
            document.getElementById("cancelButton" + formId).hidden = true;
            document.getElementById("deleteForm" + formId).hidden = true;
            document.getElementById("updateButton" + formId).hidden = false;
        }
    </script>
    <style>
        .updating-form {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
        }
    </style>
</head>
<body>

<spring:form method="post" modelAttribute="usersentity" action="welcome">
    Name: <spring:input path="name"/><br/>
    Age: <spring:input path="age"/><br/>
    <spring:button>Submit</spring:button>
</spring:form>

<br>
<h1>Список пользователей</h1>
<table>
    <tr>
        <td>Номер</td>
        <td>Имя</td>
        <td>Возраст</td>
    </tr>
    <c:forEach items="${users}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.age}</td>
            <td>
                <div class="updating-form">
                    <button id="updateButton${user.id}" onclick="showUpdateForm(${user.id})">Редактировать</button>
                    <spring:form id="updateForm${user.id}" method="post" modelAttribute="usersentity" action="welcome"
                                 hidden="true">
                        Id: <spring:input path="id" value="${user.id}"/>
                        Name: <spring:input path="name" value="${user.name}"/>
                        Age: <spring:input path="age" value="${user.age}"/>
                        <spring:button>Применить</spring:button>
                    </spring:form>
                    <button id="cancelButton${user.id}" onclick="hideUpdateForm(${user.id})" hidden="true">Отмена</button>
                    <spring:form id="deleteForm${user.id}" method="post" modelAttribute="usersentity" action="delete" hidden="true">
                        <spring:input path="id" hidden="true" value="${user.id}"/>
                        <spring:button>Удалить</spring:button>
                    </spring:form>
                </div>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
