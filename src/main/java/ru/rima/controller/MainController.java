package ru.rima.controller;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.rima.entities.UsersEntity;
import ru.rima.services.UserService;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;

@Controller
public class MainController {

    @PostConstruct
    public void init() {
        System.out.println("Zlooo");
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:spring-config.xml");
        ctx.refresh();

        UserService userService = ctx.getBean("jpaUserService", UserService.class);

        List<UsersEntity> users = userService.findAll();
        users.sort(Comparator.comparing(UsersEntity::getId));

        model.addAttribute("users", users);
        //default value
        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setId(1L);
        usersEntity.setAge(20);
        usersEntity.setName("Миша");

        model.addAttribute("usersentity", usersEntity);
        return "index";
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.POST)
    public String insertUpdateExample(@ModelAttribute("usersentity") UsersEntity usersentity, HttpServletRequest request) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:spring-config.xml");
        ctx.refresh();

        UserService userService = ctx.getBean("jpaUserService", UserService.class);

        userService.save(usersentity);
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteExample(@ModelAttribute("usersentity") UsersEntity usersentity, HttpServletRequest request) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:spring-config.xml");
        ctx.refresh();

        UserService userService = ctx.getBean("jpaUserService", UserService.class);

        userService.delete(usersentity.getId());
        return "redirect:/welcome";
    }
}
