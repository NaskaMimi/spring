package ru.rima.services;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rima.entities.UsersEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Service("jpaUserService")
@Transactional
public class UserServiceImpl implements UserService{

    @PersistenceContext
    private EntityManager entityManager;

    public List<UsersEntity> findAll() {
        return entityManager.createNamedQuery("UsersEntity.findAll", UsersEntity.class).getResultList();
    }

    public UsersEntity findById(Integer id) {
        TypedQuery<UsersEntity> query = entityManager.createNamedQuery("UsersEntity.findById", UsersEntity.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public UsersEntity save(UsersEntity user) {
        if (user.getId() == null) {
            entityManager.persist(user);
        } else {
            entityManager.merge(user);
        }
        System.out.println("Contact saved with id: " + user.getId());
        return user;
    }

    public void delete(Long userId) {
        UsersEntity usersEntity = entityManager.find(UsersEntity.class, userId);
        entityManager.remove(usersEntity);
        System.out.println("User with id: " + usersEntity.getId() + " deleted successfully");
    }
}
