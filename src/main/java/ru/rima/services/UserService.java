package ru.rima.services;

import ru.rima.entities.UsersEntity;

import java.util.List;

public interface UserService {
    public List<UsersEntity> findAll();
    public UsersEntity findById(Integer id);
    public UsersEntity save(UsersEntity user);
    public void delete(Long userId);
}
