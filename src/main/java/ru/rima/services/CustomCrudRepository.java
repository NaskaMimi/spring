package ru.rima.services;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.rima.entities.UsersEntity;

@Repository
public interface CustomCrudRepository extends CrudRepository<UsersEntity, Long> {
}
