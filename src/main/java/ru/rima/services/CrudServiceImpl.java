package ru.rima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rima.entities.UsersEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
@Service("crudUserService")
@Transactional
public class CrudServiceImpl implements CrudService {

    @Autowired
    private CustomCrudRepository crudRepository;

    @Override
    public List<UsersEntity> findAll() {
        return StreamSupport.stream(crudRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public UsersEntity findById(Long id) {
        return crudRepository.findOne(id);
    }

    @Override
    public UsersEntity save(UsersEntity user) {
        return crudRepository.save(user);
    }

    @Override
    public void delete(UsersEntity user) {
        crudRepository.delete(user);
    }
}
