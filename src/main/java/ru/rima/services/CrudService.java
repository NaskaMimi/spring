package ru.rima.services;

import ru.rima.entities.UsersEntity;

import java.util.List;

public interface CrudService {
    public List<UsersEntity> findAll();

    public UsersEntity findById(Long id);

    public UsersEntity save(UsersEntity user);

    public void delete(UsersEntity user);
}
